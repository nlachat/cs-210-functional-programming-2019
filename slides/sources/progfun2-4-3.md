% Type Classes
%
%

Type Classes
============

In the previous lectures we have seen a particular pattern of code:

~~~
trait Ordering[A] { def compare(a1: A, a2: A): Int }

object Ordering {
  given Int: Ordering[Int] { def compare(x: Int, y: Int) = x - y }
  given String: Ordering[String] {
    def compare(s: String, t: String) = s.compareTo(t)
  }
}

def sort[A: Ordering](xs: List[A]): List[A] = ...
~~~

We say that `Ordering` is a **type class**.

Polymorphism
============

Type classes provide yet another form of polymorphism:
  
The `sort` method can be called with lists containing elements of
any type `A` for which there is a given instance of type `Ordering[A]`.

At compilation-time, the compiler resolves the specific `Ordering`
implementation that matches the type of the list elements.

Exercise
========

Implement an instance of the `Ordering` typeclass for the `Rational` type.

~~~
/** A rational number
  * @param num   Numerator
  * @param denom Denominator
  */
case class Rational(num: Int, denom: Int)
~~~

Reminder:

let $q = \frac{num_q}{denom_q}$, $r = \frac{num_r}{denom_r}$,

$q < r \Leftrightarrow \frac{num_q}{denom_q} < \frac{num_r}{denom_r} \Leftrightarrow num_q \times denom_r < num_r \times denom_q$

Digression: Retroactive Extension
=================================

It is worth noting that we were able to implement the `Ordering[Rational]`
instance without changing the `Rational` class definition.

Type classes support *retroactive* extension: the ability to extend a data
type with new operations without changing the original definition of the data type.

In this example, we have added the capability of comparing `Rational` numbers.

Exercise
========

Implement an instance of the `Ordering` typeclass for pairs of type `(A, B)`.

Example of use: Consider a program for managing an address book. We would like to
sort the addresses by zip codes first and then by street name. Two addresses
with different zip codes are ordered according to their zip code, otherwise
(when the zip codes are the same) the addresses are sorted by street name.

Using Type Classes
==================

So far we have shown how to add new instances of a type class, for a specific type.

Now, let’s focus on the other side: how to use (and reason about) type classes.

For example, the `sort` function is written in terms of the `Ordering` type class,
whose implementation is itself defined by each specific instance, and is therefore
unknown at the time the `sort` function is written. If an `Ordering` instance implementation
is incorrect, then the `sort` function becomes incorrect too!

To prevent this problem to happen, type classes are often accompanied by **laws**,
which describe properties that instances must satisfy, and that users of type classes
can rely on.

Example: Laws of the `Ordering` Type Class
==========================================

Which properties do we want instances of the `Ordering` type class to satisfy
so that we can implement the `sort` function?

->

$x < y \wedge y < z \implies x < z$ (transitivity)

Example of Type Class: Ring
===========================

According to Wikipedia:

> In mathematics, a **ring** is one of the fundamental algebraic structures used
> in abstract algebra. It consists of a set equipped with two binary operations
> that generalize the arithmetic operations of addition and multiplication.
> Through this generalization, theorems from arithmetic are extended to non-numerical
> objects such as polynomials, series, matrices and functions.

By abstracting over the ring structure, developers could write programs that could
then be applied to various domains (arithmetic, polynomials, series, matrices and functions).

Ring Laws
=========

A ring is a set equipped with two binary operations, $+$ and $*$, satisfying the following
laws (called the ring axioms):

\begin{tabular}{ll}
$(a + b) + c = a + (b + c)$   & ($+$ is associative) \\
$a + b = b + a$               & ($+$ is commutative) \\
$a + 0 = a$                   & ($0$ is the additive identity) \\
$a + -a = 0$                  & ($-a$ is the additive inverse of $a$) \\
$(a * b) * c = a * (b * c)$   & ($*$ is associative) \\
$a * 1 = a$                   & ($1$ is the multiplicative identity) \\
$a * (b + c) = a * b + a * c$ & (left distributivity) \\
$(b + c) * a = b * a + c * a$ & (right distributivity)
\end{tabular}

Exercise
========

Define a `Ring` Type Class.

Implement a function that checks that the `+` associativity law is satisfied
by a given `Ring` instance.

`Numeric` Type Class
====================

In practice, the standard library already provides a type class `Numeric`,
which models a ring structure.

