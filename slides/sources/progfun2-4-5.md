% Extension Methods
%
%

Extension Methods: Motivation
=============================

In the previous lectures, we have seen that type classes could be
used to retroactively add operations to existing data types.

However, when the operations are defined outside of the data types,
they can’t be called like methods on these data type instances.

~~~
   case class Rational(num: Int, denom: Int)
   given Ordering[Rational] = ...
   val x: Rational = ...
   val y: Rational = ...

   x < y
//   ^
// value '<' is not a member of 'Rational'
~~~

Extension Methods (1)
=====================

**Extension methods** make it possible to add methods to a type after the
type is defined.

->

~~~
def (lhs: Rational) < (rhs: Rational): Boolean =
  lhs.num * rhs.denom < rhs.num * lhs.denom

val x: Rational = ...
val y: Rational = ...

x < y // It works!
~~~

Extension Methods (2)
=====================

~~~
def (lhs: Rational) < (rhs: Rational): Boolean = ...
~~~

- An extension method definition is a method definition with a parameter clause
  **before** the method name,
- The leading parameter clause must have exactly one parameter,
- Extension methods are applicable if they are visible (by being defined, inherited,
  or imported) in a scope enclosing the point of the application.

Given Extension Methods (1)
===========================

How can we add the `<` operation to any type `A` for which there is a given
`Ordering[A]` instance?

->

~~~
def [A](lhs: A) < (rhs: A)(given ord: Ordering[A]): Boolean =
  ord.lt(lhs, rhs)
~~~

->

At application site, the compiler will infer the `ord` argument
according to the operands type.

Given Extension Methods (2)
===========================

Alternatively, the `<` extension method could be directly defined in the
`Ordering` type class:

~~~
trait Ordering[A] {
  def (lhs: A) < (rhs: A): Boolean
}
~~~

->

Such extension methods are applicable if there is a given instance
visible at the point of the application.

Complete Example (1)
====================

~~~
trait Ordering[A] {
  def (lhs: A) < (rhs: A): Boolean
}
~~~

Complete Example (2)
====================

~~~
object Ordering {
  given Ordering[Int] {
    def (lhs: Int) < (rhs: Int) = lhs < rhs
  }
  given [A: Ordering]: Ordering[List[A]] {
    def (lhs: List[A]) < (rhs: List[A]) = {
      def loop(xs: List[A], ys: List[A]): Boolean = (xs, ys) match {
        case (x :: xsT, y :: ysT) =>
          if x < y then true
          else if y < x then false
          else loop(xsT, ysT)
        case (xs, ys) => xs.isEmpty && ys.nonEmpty
      }
      loop(lhs, rhs)
    }
  }
}
~~~

Complete Example (3)
====================

~~~
case class Rational(num: Int, denom: Int)

object Rational {
  given Ordering[Rational] {
    def (lhs: Rational) < (rhs: Rational) =
      lhs.num * rhs.denom < rhs.num * lhs.denom
  }
}
~~~

Complete Example (4)
====================

~~~
import Ordering.given

val i  = 1
val j  = 2
val p  = Rational(1, 2)
val q  = Rational(1, 3)
val xs = List(1, 2, 3)
val ys = List(1, 2, 4)

i < j   // true
p < q   // false
xs < ys // true
~~~

Summary
=======

In this lecture, we have seen:

- how to define extension methods outside of a type definition,
- how to define extension methods for type class instances.
