% More Functions on Lists
%
%

List Methods (1)
================

\red{Sublists and element access:}

\begin{tabular}{lp{8cm}}
   \verb`  xs.length     `  & The number of elements of \verb`xs`.
\\ \verb`  xs.last`    & The list's last element, exception if \verb`xs` is empty.
\\ \verb`  xs.init`    & A list consisting of all elements of \verb`xs` except the last one,
                         exception if \verb`xs` is empty.
\\ \verb`  xs.take(n)` & A list consisting of the first \verb`n` elements of \verb`xs`, or \verb`xs` itself
                         if it is shorter than \verb`n`.
\\ \verb`  xs.drop(n)` & The rest of the collection after taking \verb`n` elements.
\\ \verb`  xs(n)`      & (or, written out, \verb`xs.apply(n)`). The element of \verb`xs` at index \verb`n`.

\end{tabular}

List Methods (2)
================

\red{Creating new lists:}

\begin{tabular}{lp{8cm}}
   \verb`  xs ++ ys`        & The list consisting of all elements of \verb`xs`
                              followed by all elements of \verb`ys`.
\\ \verb`  xs.reverse`      & The list containing the elements of \verb`xs` in reversed order.
\\ \verb`  xs.updated(n, x)`& The list containing the same elements as \verb`xs`, except at index
                              \verb`n` where it contains \verb`x`.
\end{tabular}

\red{Finding elements:}

\begin{tabular}{lp{8cm}}
   \verb`  xs.indexOf(x)   `& The index of the first element in \verb`xs` equal to \verb`x`, or \verb`-1` if \verb`x`
                              does not appear in \verb`xs`.
\\ \verb`  xs.contains(x)  `& same as \verb`xs.indexOf(x) >= 0`
\end{tabular}

Implementation of `last`
========================

The complexity of `head` is (small) constant time.

What is the complexity of `last`?

To find out, let's write a possible implementation of `last` as a stand-alone function.

      def last[T](xs: List[T]): T = xs match
        case List() => throw Error("last of empty list")
        case List(x) =>
        case y :: ys =>

Implementation of `last`
========================

The complexity of `head` is (small) constant time.

What is the complexity of `last`?

To find out, let's write a possible implementation of `last` as a stand-alone function.

      def last[T](xs: List[T]): T = xs match
        case List() => throw Error("last of empty list")
        case List(x) => x
        case y :: ys =>

Implementation of `last`
========================

The complexity of `head` is (small) constant time.

What is the complexity of `last`?

To find out, let's write a possible implementation of `last` as a stand-alone function.

      def last[T](xs: List[T]): T = xs match
        case List() => throw Error("last of empty list")
        case List(x) => x
        case y :: ys => last(ys)
->
So, `last` takes steps proportional to the length of the list `xs`.

Exercise
========

Implement `init` as an external function, analogous to `last`.

      def init[T](xs: List[T]): List[T] = xs match
        case List() => throw Error("init of empty list")
        case List(x) => ???
        case y :: ys => ???

\quiz

Exercise
========

Implement `init` as an external function, analogous to `last`.

      def init[T](xs: List[T]): List[T] = xs match
        case List() => throw Error("init of empty list")
        case List(x) =>
        case y :: ys =>

\quiz

Exercise
========

Implement `init` as an external function, analogous to `last`.

      def init[T](xs: List[T]): List[T] = xs match
        case List() => throw Error("init of empty list")
        case List(x) => List()
        case y :: ys =>

\quiz

Exercise
========

Implement `init` as an external function, analogous to `last`.

      def init[T](xs: List[T]): List[T] = xs match
        case List() => throw Error("init of empty list")
        case List(x) => List()
        case y :: ys => y :: init(ys)

\quiz

Implementation of Concatenation
===============================

How can concatenation be implemented?

Let's try by writing a stand-alone function:

      def concat[T](xs: List[T], ys: List[T]) =

Implementation of Concatenation
===============================

How can concatenation be implemented?

Let's try by writing a stand-alone function:

      def concat[T](xs: List[T], ys: List[T]) = xs match
        case List() =>
        case z :: zs =>

Implementation of Concatenation
===============================

How can concatenation be implemented?

Let's try by writing a stand-alone function:

      def concat[T](xs: List[T], ys: List[T]) = xs match
        case List() => ys
        case z :: zs =>

Implementation of Concatenation
===============================

How can concatenation be implemented?

Let's try by writing a stand-alone function:

      def concat[T](xs: List[T], ys: List[T]) = xs match
        case List() => ys
        case z :: zs => z :: concat(zs, ys)

->

What is the complexity of `concat`?

->

Answer: `O(xs.length)`

Implementation of `reverse`
===========================

How can reverse be implemented?

Let's try by writing a stand-alone function:

     def reverse[T](xs: List[T]): List[T] = xs match
       case List() =>
       case y :: ys =>

Implementation of `reverse`
===========================

How can reverse be implemented?

Let's try by writing a stand-alone function:

     def reverse[T](xs: List[T]): List[T] = xs match
       case List() => List()
       case y :: ys =>


Implementation of `reverse`
===========================

How can reverse be implemented?

Let's try by writing a stand-alone function:

     def reverse[T](xs: List[T]): List[T] = xs match
       case List() => List()
       case y :: ys => reverse(ys) ++ List(y)

->

What is the complexity of `reverse`?

->

Answer: `O(xs.length * xs.length)`

\red{Can we do better?} (to be solved later).

Exercise
========

Remove the `n`'th element of a list `xs`. If `n` is out of bounds, return `xs` itself.

     def removeAt[T](n: Int, xs: List[T]) = ???

Usage example:

\begin{worksheet}
 \verb`removeAt(1, List('a', 'b', 'c', 'd'))` \wsf   List(a, c, d)
\end{worksheet}

Exercise (Harder, Optional)
===========================

Flatten a list structure:

     def flatten(xs: List[Any]): List[Any] = ???

     flatten(List(List(1, 1), 2, List(3, List(5, 8))))
                 >   res0: List[Any] = List(1, 1, 2, 3, 5, 8)



