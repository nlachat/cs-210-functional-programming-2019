%! TEX program = xelatex
\documentclass[aspectratio=169]{beamer}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{listings}
\input{fontsetup}
\input{scalalistings}
\input{mathsetup}
\input{mysymbols}
\input{mybeamer}

\usepackage{tikz}
\usetikzlibrary{automata,positioning,arrows,arrows.meta}
\usepackage{bclogo}

\title{Implementing a Simple Programming Language}
\author{Functional Programing (CS-210)}
\date{EPFL}

% \newcommand{\seqcircuit}[3]{% #1=S, #2=r, #3=A
% \begin{tikzpicture}[shorten >=0.8pt,node distance=2cm,on grid,auto,
%     double distance=1.5pt,
%     every state/.style={
%       semithick,
%       fill=gray!10,
%       shape=rectangle
%     },
%     every edge/.style={
%       draw,->,
%       >={Stealth[length=2.8mm,width=3.6mm,inset=2.3mm]},
%       semithick,
%       auto
%     },
%     every loop/.style={looseness=8}]
%    \node (s) {#1}; 
%    \node[state] (r) [right=of s] {#2};
%    \node (a) [above=of r] {#3}; 
%    \path [->] (s) edge node {} (r);
%    \path [->] (a) edge node {} (r);
%    \draw [->] (r) edge[out=270,in=270] node{} (s);
% \end{tikzpicture}
% }

\begin{document}
\maketitle

\newcommand{\thet}[1]{\textbf{\emph{#1}}}

\begin{frame}[fragile]
  \frametitle{Simple Untyped Functional Language}

  Example program:
\begin{lstlisting}[language=simple]
(
  def fact = (n => (if n then (* n (fact (- n 1))) else 1))
  (fact 6)
)
\end{lstlisting}
evaluates to: \pause \textbf{720}

\

\pause
\begin{lstlisting}[language=simple]
(
  def twice =  (f => x => (f (f x)))
  def square = (x => (* x x))
  (twice square 3)
)
\end{lstlisting}
evaluates to: \pause \textbf{81}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Program Representation: Abstract Syntax Trees}

\begin{lstlisting}[language=simple]
(def twice =  (f => x => (f (f x)))
 def square = (x => (* x x))
 (twice square 3))
\end{lstlisting}
$\approx$
\begin{lstlisting}[language=Scala]
val defs : DefEnv = Map[String, Expr](
  "twice" -> Fun("f", Fun("x",
                          Call(N("f"), Call(N("f"), N("x"))))),
  "square" -> Fun("x", BinOp(Times, N("x"), N("x"))))
val expr = Call(Call(N("twice"), N("square")), C(3))
\end{lstlisting}
  
\begin{itemize}
\item We represent a program using \thet{expression tree} called
  Abstract Syntax Tree (AST)
\item Our implementation is an \thet{interpreter}, which traverses
  AST to produce the result
\item We discuss later briefly how to convert an input file into an
  abstract syntax tree; \\
  more on that in the course \thet{Computer Language Processing (CS-320)}
  next year
\end{itemize}

\end{frame}

\begin{frame}[label=overview]
  \frametitle{Growing a Language and Its Interpreter}

  \begin{enumerate}
  \item[I01] Language of arithmetic and \thet{if} expressions
  \item[I02] Absolute value and its \thet{desugaring}
  \item[I03] \thet{Recursive} functions implemented using \thet{substitutions}
  \item[I04] \thet{Environment} instead of substitutions
  \item[I05] \thet{Higher-order} functions using substitutions
  \item[I06] Higher-order functions using environments
  \item[I07] \thet{Nested recursive} definitions using environments
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I01. Language of arithmetic and \thet{if} expressions: Trees}

  Integer constants combined using arithmetic operations and the \code{if} conditional
\begin{lstlisting}[language=Scala]
val expr1 = BinOp(Times, C(6), C(7))        // 6 * 7
val cond1 = BinOp(LessEq, expr1, C(50))     // expr1 <= 50
val expr2 = IfNonzero(cond1, C(10), C(20))  // if (cond1) 10 else 20
\end{lstlisting}
How to describe such trees?

\pause
\begin{lstlisting}[language=Scala]
enum Expr 
  case C(c: BigInt)                          // integer constant
  case BinOp(op: BinOps, e1: Expr, e2: Expr) // binary operation
  case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr)

enum BinOps
  case Plus, Minus, Times, Power, LessEq
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I01. Language of arithmetic and \thet{if} expressions: Printing}

\begin{lstlisting}[language=Scala]
def str(e: Expr): String = e match 
  case C(c) => c.toString
  case BinOp(op, e1, e2) =>
    s"(${strOp(op)} ${str(e1)} ${str(e2)})" // string interpolation
  case IfNonzero(cond, trueE, falseE) => 
    s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"
    
def strOp(op: BinOps): String = op match
  case Plus   => "+"
  case Minus  => "-"
  case Times  => "*"
  case Power  => "^"
  case LessEq => "<="

> str(IfNonzero(BinOp(LessEq, C(4), C(50)), C(10), C(20)))
(if (<= 4 50) then 10 else 20)
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
  \frametitle{I01. Language of arithmetic and \thet{if} expressions: Interpreting}

\begin{lstlisting}[language=Scala]
def eval(e: Expr): BigInt = e match 
  case C(c) => c
  case BinOp(op, e1, e2) =>
    evalBinOp(op)(eval(e1), eval(e2))
  case IfNonzero(cond, trueE, falseE) =>
    if eval(cond) != 0 then eval(trueE) else eval(falseE)

def evalBinOp(op: BinOps)(x: BigInt, y: BigInt): BigInt = op match
  case Plus => x + y
  case Minus => x - y
  case Times => x * y
  case Power => x.pow(y.toInt)
  case LessEq => if (x <= y) 1 else 0

> eval(IfNonzero(BinOp(LessEq, C(4), C(50)), C(10), C(20)))
10
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
  \frametitle{I02. Absolute Value and Its \thet{Desugaring}: Trees}

\begin{lstlisting}[language=Scala]
enum Expr 
  case C(c: BigInt)
  case BinOp(op: BinOps, e1: Expr, e2: Expr)
  case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr)
  case AbsValue(arg: Expr)   // new case
\end{lstlisting}  
How to extend evaluator to work with absolute value as well? Two approaches:
\begin{itemize}
\item add a case to the interpreter (exercise)
\item transform (desugar) trees to reduce them to previous cases
\end{itemize}

\

\thet{Syntactic sugar} = extra language constructs that are not strictly necessary because they can be expressed
in terms of others (they make the language sweeter to use)

\

\thet{Desugaring} = automatically eliminating syntactic sugar by expanding constructs

\end{frame}

\begin{frame}[fragile]
  \frametitle{I02. Desugaring Absolute Value: Idea}

By definition of absolute value, we would like this equality to hold:
\begin{lstlisting}[language=simple]
abs x   !*$\equiv$*!   if (<= x 0) then (- 0 x) else x
\end{lstlisting}
that is, at the level of AST,
\begin{lstlisting}[language=Scala]
AbsValue(x) !*$\leadsto$*!
   IfNonzero(BinOp(LessEq, x, C(0)), 
             BinOp(Minus, C(0), x),
             x)
\end{lstlisting}
How to write \code{desugar} function that eliminates all occurrences of \code{AbsValue}?

\ \pause

Replace (recursively) each subtree \code{AbsValue(x)} with its definition.
\end{frame}

\begin{frame}[fragile]
  \frametitle{I02. Desugaring Absolute Value: Code}

\begin{lstlisting}[language=Scala]
def desugar(e: Expr): Expr = e match
  case C(c) => e
  case BinOp(op, e1, e2) =>
       BinOp(op, desugar(e1), desugar(e2))
  case IfNonzero(cond, trueE, falseE) =>
       IfNonzero(desugar(cond), desugar(trueE), desugar(falseE))
  case AbsValue(arg) =>
    val x = desugar(arg)
    IfNonzero(BinOp(LessEq, x, C(0)), 
              BinOp(Minus, C(0), x),
              x)  
\end{lstlisting}
\end{frame}            

\begin{frame}[fragile]
  \frametitle{I02. Desugaring Absolute Value: Example Run}

\begin{lstlisting}[language=Scala]
def show(e: Expr): Unit =
  println("original:")
  println(str(e))
  val de = desugar(e)
  println("desugared:")
  println(str(de))
  println(" ~~> " + eval(de) + "\n")

show(AbsValue(BinOp(Plus,C(10),C(-50))))
\end{lstlisting}
  
\begin{lstlisting}[language=simple]
original:
(abs (+ 10 -50))
desugared:
(if (<= (+ 10 -50) 0) then (- 0 (+ 10 -50)) else (+ 10 -50))
  ~~> 40
\end{lstlisting}

\end{frame}

\againframe{overview}

\begin{frame}[fragile]
  \frametitle{I03: Recursive Functions}

  We would like to handle examples like this one:
\begin{lstlisting}[language=simple]
def fact n =
  (if n then (* n (fact (- n 1))) else 1)
(fact 6)
\end{lstlisting}
What do we need to add to our abstract syntax trees?
\pause
\begin{itemize}
\item names inside expressions to refer to parameters (\code{n})
\item calls to user-defined functions (\code{fact 6})
\item definitions (map function names to parameters and function bodies) 
\end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I03: Recursive Function Definitions: Trees and Factorial Example}
  
\begin{lstlisting}[language=Scala]
enum Expr 
  case C(c: BigInt) 
  case N(name: String)   // immutable variable
  case BinOp(op: BinOps, e1: Expr, e2: Expr) 
  case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
  case Call(function: String, args: List[Expr])   // function call

case class Function(params: List[String], body: Expr) 
type DefEnv = Map[String, Function]  // function names to definitions

val defs : DefEnv = Map[String, Function](
  "fact" -> Function(List("n"), // formal parameter "n", body:
    IfNonzero(N("n"),
              BinOp(Times, N("n"),
                    Call("fact", List(BinOp(Minus,(N("n"), C(1)))))),
              C(1)))
) // if n then (* n (fact (- n 1))) else 1
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I03: Idea of Evaluation Based Using Substitution}
  \begin{itemize}
  \item evaluate arguments so they become constants
  \item look up function body, replace formal parameters with constants
  \item evaluate replaced function body
  \end{itemize}
  
\begin{lstlisting}[language=simple]
def fact n = (if n then (* n (fact (- n 1))) else 1)
(fact 3)
(if 3 then (* 3 (fact (- 3 1))) else 1)
|  (fact 2)
|  (if 2 then (* 2 (fact (- 2 1))) else 1)
|  |  (fact 1)
|  |  (if 1 then (* 1 (fact (- 1 1))) else 1)
|  |  |  (fact 0)
|  |  |  (if 0 then (* 0 (fact (- 0 1))) else 1)
|  |  |  +--> 1
|  |  +--> 1
|  +--> 2
+--> 6
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I03: eval Using Substitution}

\begin{lstlisting}[language=Scala]
def eval(e: Expr): BigInt = e match
  case C(c) => c
  case N(n) => error(s"Unknown name '$n'") // should never occur
  case BinOp(op, e1, e2) =>
    evalBinOp(op)(eval(e1), eval(e2))
  case IfNonzero(cond, trueE, falseE) =>
    if eval(cond) != 0 then eval(trueE)
    else eval(falseE)
  case Call(fName, args) => // the only new case we handle
    defs.get(fName) match // defs is a global map with all functions
      case Some(f) => // f has body:Expr and params:List[String]
        val evaledArgs = args.map((e: Expr) => C(eval(e)))   
        val bodySub = substAll(f.body, f.params, evaledArgs)
        eval(bodySub)   // may contain further recursive calls
                        // bodySub should no longer have N(...)
\end{lstlisting}
\end{frame}
\begin{frame}[fragile]
  \frametitle{I03: Substitution}

\begin{lstlisting}[language=Scala]
// substitute all n with r in expression e
def subst(e: Expr, n: String, r: Expr): Expr = e match
  case C(c) => e
  case N(s) => if s==n then r else e
  case BinOp(op, e1, e2) => 
       BinOp(op, subst(e1,n,r), subst(e2,n,r))
  case IfNonzero(c, trueE, falseE) =>
       IfNonzero(subst(c,n,r), subst(trueE,n,r), subst(falseE,n,r))
  case Call(f, args) => 
       Call(f, args.map(subst(_,n,r)))

def substAll(e: Expr, names: List[String],
               replacements: List[Expr]): Expr  = 
  (names, replacements) match 
     case (n :: ns, r :: rs) => substAll(subst(e,n,r), ns, rs)
     case _ => e
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
  \frametitle{I03: Division Example and Wrap Up}
\begin{lstlisting}[language=simple]
def div x y =
  (if (<= y x) then (+ 1 (div (- x y) y)) else 0)

(div 15 6)
(if (<= 6 15) then (+ 1 (div (- 15 6) 6)) else 0)
|  (div 9 6)
|  (if (<= 6 9) then (+ 1 (div (- 9 6) 6)) else 0)
|  |  (div 3 6)
|  |  (if (<= 6 3) then (+ 1 (div (- 3 6) 6)) else 0)
|  |  +--> 0
|  +--> 1
+--> 2
\end{lstlisting}

\pause
This completes the interpreter for recursive computable functions.
Every computable function that maps an $n$-tuple of integers
into an integer can be described in it and our interpreter can execute it!
We can even encode data structures as large integers.
\end{frame}

\againframe{overview}

\begin{frame}[fragile]
  \frametitle{I04: Environment instead of substitutions}

  \thet{Environments} are often more efficient alternative to substitutions.
  
  Instead of copying body of function definition and replacing
  parameter names with argument constants, we do replacement lazily:
  \begin{itemize}
  \item leave the body as is (no copying!)
  \item record map from names to argument constants in the environment
  \item when we find a name, look it up in the environment
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I04: Factorial Using Environments}
\begin{lstlisting}[language=simple]
fact(3)
|  env: Map(n -> 3)
|  (if n then (* n (fact (- n 1))) else 1)        // body as declared
|  fact(2)
|  |  env: Map(n -> 2)
|  |  (if n then (* n (fact (- n 1))) else 1)     // same
|  |  fact(1)
|  |  |  env: Map(n -> 1)
|  |  |  (if n then (* n (fact (- n 1))) else 1)  // still same
|  |  |  fact(0)
|  |  |  |  env: Map(n -> 0)
|  |  |  |  (if n then (* n (fact (- n 1))) else 1)  // again same!
|  |  |  +--> 1
|  |  +--> 1
|  +--> 2
+--> 6
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I03: Evaluation Using Environment}

\begin{lstlisting}[language=Scala]
def evalE(e: Expr, env: Map[String, BigInt]): BigInt = e match
  case C(c) => c
  case N(n) => env(n)  // look up name in the environment
  case BinOp(op, e1, e2) =>
    evalBinOp(op)(evalE(e1, env), evalE(e2, env))
  case IfNonzero(cond, trueE, falseE) =>
    if evalE(cond,env) != 0 then evalE(trueE,env)
    else evalE(falseE,env)
  case Call(fName, args) => 
    defs.get(fName) match 
      case Some(f) => 
        val evaledArgs = args.map((e: Expr) => evalE(e,env))
        // newEnv additionally maps parameters to arguments
        val newEnv = env ++ f.params.zip(evaledArgs)
        evalE(f.body, newEnv) 
\end{lstlisting}
\end{frame}

\againframe{overview}

\begin{frame}[fragile]
  \frametitle{I05: Higher-Order Functions Using Substitution}
\scalebox{0.8}{
\lstinputlisting{twicesquare.simple}
}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: Trees for Higher-Order Functions}

Now we have a case for creating function anywhere in the expression
(param => body)

Argument of function call need not be a name but can be an expression

A function has exactly one argument (use currying if needed)

\begin{lstlisting}[language=Scala]
enum Expr 
  case C(c: BigInt)
  case N(name: String) 
  case BinOp(op: BinOps, e1: Expr, e2: Expr) 
  case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
  case Call(fun: Expr, arg: Expr)     // fun can be expression itself
  case Fun(param: String, body: Expr) // param => body


Call(Fun("x", BinOp(Times, N("x"), N("x"))),  //  x => (* x x)
     C(3))                                    //  3
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: Eval Using Substitution}

  Result can be a function, so we return an Expr (not BigInt)
  
\begin{lstlisting}[language=Scala]
def eval(e: Expr): Expr = e match
  case C(c) => e
  case N(n) => eval(defs(n)) // find in global defs, then eval
  case BinOp(op, e1, e2) =>
    evalBinOp(op)(eval(e1), eval(e2))
  case IfNonzero(cond, trueE, falseE) =>
    if eval(cond) != C(0) then eval(trueE)
    else eval(falseE)
  case Fun(_,_) => e  // functions evaluate to themselves
  case Call(fun, arg) =>
    eval(fun) match 
      case Fun(n,body) => eval(subst(body, n, eval(arg)))
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: Substitution on Trees for Higher-Order Functions}

\begin{lstlisting}[language=Scala]
// substitute all n with r in expression e
def subst(e: Expr, n: String, r: Expr): Expr = e match
  case C(c) => e
  case N(s) => if s==n then r else e
  case BinOp(op, e1, e2) => 
       BinOp(op, subst(e1,n,r), subst(e2,n,r))
  case IfNonzero(cond, trueE, falseE) =>
       IfNonzero(subst(cond,n,r), subst(trueE,n,r), subst(falseE,n,r))    
  case Call(f, arg) => 
       Call(subst(f,n,r), subst(arg,n,r))
  case Fun(formal,body) =>
       if formal==n then e  // do not substitute under (n => ...)
       else Fun(formal, subst(body,n,r)) 
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: More Examples: Twice Factorial}

\begin{lstlisting}[language=simple]
(def twice =  (f => x => (f (f x)))
 def fact n = (if n then (* n (fact (- n 1))) else 1)
 (twice fact 3))
~~> 720
\end{lstlisting}

\pause
\begin{lstlisting}[language=simple]
(def twice1 =  (f => fact => (f (f fact)))
 def fact n = (if n then (* n (fact (- n 1))) else 1)
 (twice1 fact 3))
\end{lstlisting}
\pause
\begin{lstlisting}[language=simple]
  FUN: (f => (fact => (f (f fact))))  
  ARG: (n => (if n then (* n (fact (- n 1))) else 1))
  FUN: (fact => ((n => (if n then (* n (fact (- n 1))) else 1)) 
            ((n => (if n then (* n (fact (- n 1))) else 1)) fact)))
  ARG: 3
  (if 3 then (* 3 (3 (- 3 1))) else 1)
  (3 (- 3 1))
 java.lang.Exception: Cannot apply non-function 3 in a call
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: Variable Capture}

\begin{lstlisting}[language=Scala]
def subst(e: Expr, n: String, r: Expr): Expr = e match
  ...
  case Fun(formal,body) =>
       if formal==n then e  // do not substitute under (n => ...)
       else Fun(formal, subst(body,n,r))
\end{lstlisting}
The last line exhibits \thet{variable capture problem}.

If 'formal' occurs free in 'r', then it will be captured
by Fun(formal,...) even though that outside
occurrence of 'formal' in 'r' has nothing to do with the bound variable
in the anonymous function.
\begin{lstlisting}[language=simple]
  FUN: (f => (fact => (f (f fact))))  
  ARG: (n => (if n then (* n (fact (- n 1))) else 1))
fact => ((n => (if n then (* n (fact (- n 1))) else 1)) 
           ((n => (if n then (* n (fact (- n 1))) else 1))  fact))
\end{lstlisting}
When we supply the integer argument
we will also substitute it instead of the name of the factorial function,
resulting in run-time error (or, in other cases, wrong result).
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: We Want to Rename Bound Variable to Avoid Capture}

In situation like this:
\begin{lstlisting}[language=simple]
  FUN: (f => (fact => (f (f fact))))  
  ARG: (n => (if n then (* n (fact (- n 1))) else 1))
\end{lstlisting}
when substituting ARG inside the body of FUN, we first rename
bound variable in FUN body into one that does not occur in ARG:
\begin{lstlisting}[language=simple]
  FUN: (f => (fact' => (f (f fact'))))  
  ARG: (n => (if n then (* n (fact (- n 1))) else 1))
\end{lstlisting}
Instead of fact' we can choose any fresh name for bound variable!

Result then evaluates correctly:
\begin{lstlisting}[language=simple]
fact' => ((n => (if n then (* n (fact (- n 1))) else 1)) 
           ((n => (if n then (* n (fact (- n 1))) else 1))  fact'))
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: Renaming Bound Variables in Subst}

We call our previous substitution \thet{naive substitution}:
\begin{lstlisting}[language=Scala]
def subst0(e: Expr, n: String, r: Expr): Expr = e match ...
  case Fun(formal,body) =>
       if formal==n then e else Fun(formal, subst0(body,n,r))
\end{lstlisting}

To avoid problems, we use \thet{capture-avoiding substitution}:
\begin{lstlisting}[language=Scala] 
def subst(e: Expr, n: String, r: Expr): Expr = e match ...
  case Fun(formal,body) =>
       if formal==n then e else 
         val fvs = freeVars(r)
         val (formal1, body1) =
           if fvs.contains(formal) then // rename bound formal
             val formal1 = differentName(formal, fvs)
             (formal1, subst0(body, formal, N(formal1)))
           else (formal, body)
         Fun(formal1, subst(body1,n,r)) // substitute either way
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I05: Free Variables and Finding a Different Name}

\begin{lstlisting}[language=Scala] 
def differentName(n: String, s: Set[String]): String =
  if s.contains(n) then differentName(n + "'", s)
  else n

def freeVars(e: Expr): Set[String] = e match
  case C(c) => Set()
  case N(s) => Set(s)
  case BinOp(op, e1, e2) => freeVars(e1) ++ freeVars(e2)
  case IfNonzero(cond, trueE, falseE) =>
       freeVars(cond) ++ freeVars(trueE) ++ freeVars(falseE)
  case Call(f, arg) => freeVars(f) ++ freeVars(arg)
  case Fun(formal,body) => freeVars(body) - formal 
\end{lstlisting}
\end{frame}

\againframe{overview}

\begin{frame}[fragile]
  \frametitle{I06: Higher-Order Functions Using Environments}

  Environments are more efficient (and avoid variable capture more easily).

  How to use them when parameters can be functions?

\
  
  Before higher-order functions, environment mapped names to integers.

  Now, it maps names to value, which may also be functions:
\begin{lstlisting}[language=Scala] 
enum Value
  case I(i: BigInt)
  case F(f: Value => Value)

type Env = Map[String, Value]
\end{lstlisting}

We represent function values of the language we are
interpreting using functions in Scala. We say our
interpreter is \emph{meta circular} because we use features
in meta language in which we write interpreter (Scala) to
represent features of the language we are interpreting.
\end{frame}

\begin{frame}[fragile]
  \frametitle{I06: Environment-Based Interpreter is Very Concise!}

\begin{lstlisting}[language=Scala]  
def evalEnv(e: Expr, env: Map[String, Value]): Value = e match
  case C(c) => Value.I(c)
  case N(n) => env.get(n) match
    case Some(v) => v
    case None => evalEnv(defs(n), env)
  case BinOp(op, arg1, arg2) =>
    evalBinOp(op)(evalEnv(arg1,env), evalEnv(arg2,env))
  case IfNonzero(cond, trueE, falseE) =>
    if evalEnv(cond,env) != Value.I(0) then evalEnv(trueE,env)
    else evalEnv(falseE,env)
  case Fun(n,body) => Value.F{(v: Value) =>
    evalEnv(body, env + (n -> v)) } // no danger of capture
  case Call(fun, arg) => evalEnv(fun,env) match 
    case Value.F(f) => f(evalEnv(arg,env))
\end{lstlisting}  
\end{frame}     

\againframe{overview}

\begin{frame}[fragile]
  \frametitle{I07: \thet{Nested recursive} definitions using environments}

  So far we used a special global environment defs to express recursion

  We could create locally anonymous functions, but without a way to call
  them recursively.

  In this step of the interpreter, we introduce the Defs expression case for
  adding (nested, local) mutually recursive functions:

\begin{lstlisting}[language=Scala]  
enum Expr 
  case C(c: BigInt)
  case N(name: String) 
  case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
  case Call(function: Expr, arg: Expr)
  case Fun(param: String, body: Expr)
  case Defs(defs: List[(String, Expr)], rest: Expr)
\end{lstlisting}
\pause

\begin{lstlisting}[language=Scala]  
type Env = String => Option[Value]
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I07: Eval for Nested Recursive Definitions: Key Cases}

\begin{lstlisting}[language=Scala]  
def evalEnv(e: Expr, env: Env): Value = e match ...
  case N(n) => env(n) match // no use of defs, only env
    case Some(v) => v
  case Fun(n,body) => Value.F{(v: Value) => // same as before
    val env1: String => Option[Value] = 
      (s:String) => if s==n then Some(v) else env(s)
    evalEnv(body, env1) }
  case Call(fun, arg) => evalEnv(fun,env) match // same
    case Value.F(f) => f(evalEnv(arg,env))
  case Defs(defs, rest) => //
    def env1: Env = // extended environment
      (s:String) =>
        lookup(defs, s) match // list lookup in local defs
          case None => env(s) // fall back to outer scope
          case Some(body) => Some(evalEnv(body, env1)) // rec
    evalEnv(rest, env1)  
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I07: What Behavior Would We Get with This Version}

\begin{lstlisting}[language=Scala]  
def evalEnv(e: Expr, env: Env): Value = e match ...
  case N(n) => env(n) match // no use of defs, only env
    case Some(v) => v
  case Fun(n,body) => Value.F{(v: Value) => // same as before
    val env1: String => Option[Value] = 
      (s:String) => if s==n then Some(v) else env(s)
    evalEnv(body, env1) }
  case Call(fun, arg) => evalEnv(fun,env) match // same
    case Value.F(f) => f(evalEnv(arg,env))
  case Defs(defs, rest) => //
    def env1: Env = // extended environment
      (s:String) =>
        lookup(defs, s) match // list lookup in local defs
          case None => env(s) // fall back to outer scope
          case Some(body) => Some(evalEnv(body, env)) // nonrec
    evalEnv(rest, env1)  
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I07: What Behavior Would We Get with This Version}

\begin{lstlisting}[language=Scala]  
def evalEnv(e: Expr, env: Env): Value = e match ...
  case Defs(defs, rest) => //
    def env1: Env = // extended environment
      (s:String) =>
        lookup(defs, s) match // list lookup in local defs
          case None => env(s) // fall back to outer scope
          case Some(body) => Some(evalEnv(body, env)) // nonrec
    evalEnv(rest, env1)  
\end{lstlisting}

\begin{lstlisting}[language=simple]
(def fact = (n => (if n then (* n (fact (- n 1))) else 1))
 (fact 6))

java.lang.Exception: Unknown name 'fact' in top-level environment
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I07: Must Make env1 Recursive}

\begin{lstlisting}[language=Scala]  
def evalEnv(e: Expr, env: Env): Value = e match ...
  case Defs(defs, rest) => //
    def env1: Env = // extended environment
      (s:String) =>
        lookup(defs, s) match // list lookup in local defs
          case None => env(s) // fall back to outer scope
          case Some(body) => Some(evalEnv(body, env1)) // rec
    evalEnv(rest, env1)  
\end{lstlisting}

\begin{lstlisting}[language=simple]
(def fact = (n => (if n then (* n (fact (- n 1))) else 1))
 (fact 6))

~~> 720
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{I07: Initial Environment Replaces BinOp-s}

We start evaluation in the initial environment:
\begin{lstlisting}[language=Scala]
  evalEnv(e, initEnv)

  val initEnv: Env = 
    (s:String) => s match
      case "+"  => lift2int(_ + _)
      case "-"  => lift2int(_ - _)
      case "*"  => lift2int(_ * _)
      case "^"  => lift2int((x:BigInt,y:BigInt) => x.pow(y.toInt))
      case "<=" => lift2int(
        (x:BigInt,y:BigInt) => if x <= y then 1 else 0)
      case _ => error(s"Unknown name '$s' in initial environment")
\end{lstlisting}
We no longer need BinOp as special expression form
\end{frame}

\begin{frame}[fragile]
  \frametitle{I07: Lifting Binary Functions to Work on Values}

\begin{lstlisting}[language=Scala]  
  def lift2int(f: (BigInt, BigInt) => BigInt): Option[Value] =
    import Value._
    Some(F(
      (v1:Value) => F(
        (v2:Value) => {
          (v1,v2) match 
            case (I(i1),I(i2)) => I(f(i1,i2))
            case _ => error("Wrong operator type")
        })))
\end{lstlisting}
\end{frame}      

\againframe{overview}

\begin{frame}[fragile]
  \frametitle{I07: Write Tail-Recursive Division with Nested Auxiliary Function}

\pause
\begin{lstlisting}[language=Scala]
val lDefs = List[(String, Expr)](
  "nDiv" -> Fun("xCurrent", Fun("acc",
     IfNonzero(leq(N("y"), N("xCurrent")),
       Call(Call(N("nDiv"), minus(N("xCurrent"), N("y"))),
            plus(C(1), N("acc"))),
       N("acc"))))
)  
val gDefs = List[(String, Expr)]( 
  "tDiv" -> Fun("x", Fun("y",
     Defs(lDefs, Call(Call(N("nDiv"), N("x")), C(0)))))
)
val expr = Defs(gDefs,
                Call(Call(N("tDiv"), C(720)), C(10)))
\end{lstlisting}
              
\end{frame}


\end{document}
