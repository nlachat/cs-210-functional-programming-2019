package week11
class BankAccount with
  private var balance: Int = 0

  def deposit(amount: Int): Unit =
    if amount > 0 then balance = balance + amount

  def withdraw(amount: Int): Int =
    if 0 < amount && amount <= balance then
      balance = balance - amount
      balance
    else error("insufficient funds")
