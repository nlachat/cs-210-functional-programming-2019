package week14

import akka.actor.Actor
import akka.pattern.CircuitBreaker
import akka.actor.ActorRef
import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout

class Retriever(userService: ActorRef) extends Actor {
  implicit val timeout = Timeout(2.seconds)
  val cb = CircuitBreaker(context.system.scheduler,
    maxFailures = 3,
    callTimeout = 1.second,
    resetTimeout = 30.seconds)

  def receive = {
    case "get" =>
      val result = cb.withCircuitBreaker(userService ? "get").mapTo[String]
  }
}
