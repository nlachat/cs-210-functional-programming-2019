## Functional Programming 2019: Interpreter

This interpreter is for the lecture prepared by Viktor Kuncak,
based on a previous lecture of Martin Odersky in Scala 2, which 
did an interpreter for Scheme with Any as the data type for trees
and that was likely based on 
Structure and Interpretation of Computer Programs.

The present interpreter is for Dotty and uses explicitly declared
enums for trees.

### Usage

Run `sbt` and then enter `run` to be offered a choice of one of versions
of the interpreter to run.

`sbt console` will start a Dotty REPL.

For sources please study `src/main/scala`.

For more information on the sbt-dotty plugin, see the
[dotty-example-project](https://github.com/lampepfl/dotty-example-project/blob/master/README.md).
