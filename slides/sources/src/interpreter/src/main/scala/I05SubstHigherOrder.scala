object I05SubstHigherOrder 
  enum Expr 
    case C(c: BigInt)
    case N(name: String) 
    case BinOp(op: BinOps, e1: Expr, e2: Expr) 
    case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
    case Call(fun: Expr, arg: Expr)
    case Fun(param: String, body: Expr)
  end Expr
  import Expr._

  enum BinOps
    case Plus, Minus, Times, Power, LessEq
  import BinOps._

  def evalBinOp(op: BinOps)(ex: Expr, ey: Expr): Expr = (op,ex,ey) match
    case (Plus,C(x),C(y)) =>  C(x + y)
    case (Minus,C(x),C(y)) => C(x - y)
    case (Times,C(x),C(y)) => C(x * y)
    case (Power,C(x),C(y)) => C(x.pow(y.toInt))
    case (LessEq,C(x),C(y)) => C(if (x <= y) 1 else 0)
    case _ => error(s"Type error in $ex ${strOp(op)} $ey")
  
  type DefEnv = Map[String, Expr]

  def minus(e1: Expr, e2: Expr) = BinOp(Minus, e1, e2)
  def plus(e1: Expr, e2: Expr) = BinOp(Plus, e1, e2)
  def leq(e1: Expr, e2: Expr) = BinOp(LessEq, e1, e2)
  def times(e1: Expr, e2: Expr) = BinOp(Times, e1, e2)

  val defs : DefEnv = Map[String, Expr](
    "fact" -> Fun("n",
       IfNonzero(N("n"),
         times(N("n"), 
               Call(N("fact"), minus(N("n"), C(1)))),
         C(1))),
    "div" -> Fun("x", Fun("y",
       IfNonzero(BinOp(LessEq, N("y"), N("x")),
         plus(C(1), 
              Call(Call(N("div"), minus(N("x"), N("y"))),
                        N("y"))),
         C(0)))),
    "twice" -> Fun("f", Fun("y",
      Call(N("f"), Call(N("f"), N("y"))))),
    "twice1" -> Fun("f", Fun("fact",
      Call(N("f"), Call(N("f"), N("fact"))))), 
    "square" -> Fun("x", BinOp(Times, N("x"), N("x")))
  )
  val factExpr1 = Call(N("fact"), C(6))
  val divExpr2 = Call(Call(N("div"), C(15)), C(6))
  val expr3 = Call(Call(N("div"), factExpr1), C(10))
  val expr4 = Call(Call(N("twice"), N("square")), C(3))

  def eval(e: Expr): Expr = e match
    case C(c) => e
    case N(n) => 
      defs.get(n) match
        case None => error(s"Unknown name $n")
        case Some(body) => eval(body)
    case BinOp(op, e1, e2) =>
      evalBinOp(op)(eval(e1), eval(e2))
    case IfNonzero(cond, trueE, falseE) =>
      if eval(cond) != C(0) then eval(trueE)
      else eval(falseE)
    case Fun(_,_) => e
    case Call(fun, arg) =>
      Logger.log(str(e))
      Logger.push          
      val eFun = eval(fun)
      val eArg = eval(arg)
      eFun match 
        case Fun(n,body) =>
          Logger.pop
          Logger.log(s"FUN: ${str(eFun)}  ARG: ${str(eArg)}")
          val bodySub = subst(body, n, eArg)
          Logger.log(s"${str(bodySub)}")
          Logger.push
          val res = eval(bodySub)
          Logger.pop
          Logger.log(s"+--> ${str(res)}")
          res
        case _ => error(s"Cannot apply non-function ${str(eFun)} in a call")

  def subst(e: Expr, n: String, r: Expr): Expr = e match
     case C(c) => e
     case N(s) => if s==n then r else e
     case BinOp(op, e1, e2) => 
          BinOp(op, subst(e1, n, r), subst(e2,n,r))
     case IfNonzero(cond, trueE, falseE) =>
          IfNonzero(subst(cond,n,r), subst(trueE,n,r), subst(falseE,n,r))    
     case Call(f, arg) => 
          Call(subst(f,n,r), subst(arg,n,r))
     case Fun(formal,body) =>
          if formal==n then e else 
            val fvs = freeVars(r)
            val (formal1, body1) =
              if fvs.contains(formal) then // rename bound formal
                val formal1 = differentName(formal, fvs)
                (formal1, subst0(body, formal, N(formal1)))
              else (formal, body)
            Fun(formal1, subst(body1,n,r)) // substitute either way

  def differentName(n: String, s: Set[String]): String =
    if s.contains(n) then differentName(n + "'", s)
    else n

  def freeVars(e: Expr): Set[String] = e match
    case C(c) => Set()
    case N(s) => Set(s)
    case BinOp(op, e1, e2) => freeVars(e1) ++ freeVars(e2)
    case IfNonzero(cond, trueE, falseE) =>
         freeVars(cond) ++ freeVars(trueE) ++ freeVars(falseE)
    case Call(f, arg) => freeVars(f) ++ freeVars(arg)
    case Fun(formal,body) => freeVars(body) - formal 

  // naive substitution
  def subst0(e: Expr, n: String, r: Expr): Expr = e match
   case C(c) => e
   case N(s) => if s==n then r else e
   case BinOp(op, e1, e2) => 
        BinOp(op, subst0(e1, n, r), subst0(e2,n,r))
   case IfNonzero(cond, trueE, falseE) =>
        IfNonzero(subst0(cond,n,r), subst0(trueE,n,r), subst0(falseE,n,r))    
   case Call(f, arg) => 
        Call(subst0(f,n,r), subst0(arg,n,r))
   case Fun(formal,body) =>
        if formal==n then e
        else Fun(formal, subst0(body,n,r))  
          
  def error(s: String) = throw Exception(s)

  // Printing and displaying

  def strOp(op: BinOps): String = op match
    case Plus => "+"
    case Minus => "-"
    case Times => "*"
    case Power => "^"
    case LessEq => "<="
  
  def str(e: Expr): String = e match 
    case C(c) => c.toString
    case N(n) => n    
    case BinOp(op, e1, e2) =>
      s"(${strOp(op)} ${str(e1)} ${str(e2)})"
    case IfNonzero(cond, trueE, falseE) =>
      s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"
    case Call(f, arg) => "(" + str(f) + " " + str(arg) + ")"
    case Fun(n, body) => s"($n => ${str(body)})"

  def strEnv(env: Map[String, Expr]): String = 
    val defStrs = 
      for ((name,body) <- env)
      yield s"def $name =\n  ${str(body)}"
    defStrs.mkString("\n\n") + "\n"

  def show(e: Expr, log: Boolean = false): Unit =
    if log then Logger.on else Logger.off
    println(str(e))
    println(" ~~> " + str(eval(e)) + "\n")
    if log then Logger.off

  def main(args: Array[String]): Unit =    
    show(Call(Fun("x", BinOp(Times, N("x"), N("x"))), C(3)),true)
    println(strEnv(defs))
    show(factExpr1)
    show(divExpr2)
    show(expr3)    
    show(expr4, true) 
    show(Call(Call(N("twice1"), N("fact")), C(3)), true)

end I05SubstHigherOrder