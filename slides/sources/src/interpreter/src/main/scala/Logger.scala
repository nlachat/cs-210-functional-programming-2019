object Logger
  var logging = false
  var indent = 0
  def on =
    logging = true
    indent = 0
  def off =
    logging = false
  def push =
    indent = indent + 1
  def pop =
    indent = indent - 1
  def log(s: String): Unit =
    if logging then println("|  " * indent + s)
    else ()
end Logger
