object I01ExpressionLanguage

  enum Expr 
    case C(c: BigInt)
    case BinOp(op: BinOps, e1: Expr, e2: Expr)
    case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr)
  end Expr
  import Expr._

  enum BinOps
    case Plus, Minus, Times, Power, LessEq
  import BinOps._

  def evalBinOp(op: BinOps)(x: BigInt, y: BigInt): BigInt = op match
    case Plus => x + y
    case Minus => x - y
    case Times => x * y
    case Power => x.pow(y.toInt)
    case LessEq => if (x <= y) 1 else 0

  def eval(e: Expr): BigInt = e match 
    case C(c) => c
    case BinOp(op, e1, e2) =>
      evalBinOp(op)(eval(e1), eval(e2))
    case IfNonzero(cond, trueE, falseE) =>
      if eval(cond) != 0 then eval(trueE)
      else eval(falseE)
  
  val expr1 = BinOp(Times, C(6), C(7))       // 6*7
  val cond1 = BinOp(LessEq, expr1, C(50))    // expr1 < 50
  val expr2 = IfNonzero(cond1, C(10), C(20)) // if (cond1) 10 else 20
  val expr3 = BinOp(Power, C(10), C(100))    // 10^100

  def strOp(op: BinOps): String = op match
    case Plus => "+"
    case Minus => "-"
    case Times => "*"
    case Power => "^"
    case LessEq => "<="
  
  def str(e: Expr): String = e match 
    case C(c) => c.toString
    case BinOp(op, e1, e2) =>
      s"(${strOp(op)} ${str(e1)} ${str(e2)})"
    case IfNonzero(cond, trueE, falseE) =>
      s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"

  def show(e: Expr): Unit =
    println(str(e))
    println(" ~~> " + eval(e) + "\n")
  
  def main(args: Array[String]): Unit = 
    show(IfNonzero(BinOp(LessEq, C(4), C(50)), C(10), C(20)))
    show(expr1)
    show(expr2)
    show(expr3)

end I01ExpressionLanguage
