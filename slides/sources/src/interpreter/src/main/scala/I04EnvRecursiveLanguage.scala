object I04EnvRecursiveLanguage 

  enum Expr 
    case C(c: BigInt) 
    case N(name: String) 
    case BinOp(op: BinOps, e1: Expr, e2: Expr) 
    case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
    case Call(function: String, args: List[Expr]) 
  end Expr
  import Expr._

  enum BinOps
    case Plus, Minus, Times, Power, LessEq
  import BinOps._

  def evalBinOp(op: BinOps)(x: BigInt, y: BigInt): BigInt = op match
    case Plus => x + y
    case Minus => x - y
    case Times => x * y
    case Power => x.pow(y.toInt)
    case LessEq => if (x <= y) 1 else 0

  case class Function(params: List[String], body: Expr)
  
  type DefEnv = Map[String, Function]

  def minus(e1: Expr, e2: Expr) = BinOp(Minus, e1, e2)
  def plus(e1: Expr, e2: Expr) = BinOp(Plus, e1, e2)
  def leq(e1: Expr, e2: Expr) = BinOp(LessEq, e1, e2)
  def times(e1: Expr, e2: Expr) = BinOp(Times, e1, e2)

  val defs : DefEnv = Map[String, Function](
    "fact" -> Function(List("n"),
      IfNonzero(N("n"),
                BinOp(Times, N("n"), Call("fact", List(BinOp(Minus,N("n"), C(1))))),
                C(1))),
    "div" -> Function(List("x", "y"),
      IfNonzero(BinOp(LessEq, N("y"), N("x")),
         plus(C(1), Call("div", List(minus(N("x"), N("y")), N("y")))),
         C(0)))
  )
  val factExpr1 = Call("fact", List(C(6)))
  val divExpr2 = Call("div", List(C(15), C(6)))
  val expr3 = Call("div", List(factExpr1, C(10)))

  def lookup(n: String, env: Map[String, BigInt]): BigInt =
    // Logger.log(s"Looking up $n in ${env.toString}")
    env(n)  

  def evalE(e: Expr, env: Map[String, BigInt]): BigInt = 
   e match
    case C(c) => c
    case N(n) => lookup(n, env)
    case BinOp(op, e1, e2) =>
      evalBinOp(op)(evalE(e1, env), evalE(e2, env))
    case IfNonzero(cond, trueE, falseE) =>
      if evalE(cond,env) != 0 then evalE(trueE,env)
      else evalE(falseE,env)
    case Call(fName, args) => 
      defs.get(fName) match 
        case None => error(s"Unknown function $fName")
        case Some(f) => 
          val evaledArgs = args.map((e: Expr) => evalE(e,env))
          Logger.log(s"$fName(${evaledArgs.mkString(",")})")
          Logger.push
          val newEnv = env ++ f.params.zip(evaledArgs)
          Logger.log("env: " + newEnv.toString)
          Logger.log(s"${str(f.body)}")
          val res = evalE(f.body, newEnv)
          Logger.pop
          Logger.log(s"+--> $res")
          res
  end evalE

  def eval0(e: Expr) = evalE(e, Map[String, BigInt]())

  def error(s: String) = throw Exception(s)

  // Printing and displaying

  def strOp(op: BinOps): String = op match
    case Plus => "+"
    case Minus => "-"
    case Times => "*"
    case Power => "^"
    case LessEq => "<="
  
  def str(e: Expr): String = e match
    case C(c) => c.toString
    case N(n) => n    
    case BinOp(op, e1, e2) =>
      s"(${strOp(op)} ${str(e1)} ${str(e2)})"
    case IfNonzero(cond, trueE, falseE) =>
      s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"
    case Call(f, args) =>
      s"($f ${args.map(str).mkString(" ")})"

  def strFun(fun: Function): String =
    s"(${fun.params.mkString(",")}) =\n  ${str(fun.body)}"

  def strEnv(env: DefEnv): String = 
    val defStrs = 
      for ((name,body) <- env)
      yield ("def " + name + strFun(body))
    defStrs.mkString("\n\n") + "\n"

  def show(e: Expr, log: Boolean = false): Unit =
    if log then Logger.on else Logger.off
    println(str(e))
    println(" ~~> " + eval0(e) + "\n")
    if log then Logger.off

  def main(args: Array[String]): Unit =    
    println(strEnv(defs))
    show(Call("fact", List(C(3))), true)
    show(factExpr1)
    show(divExpr2, true)
    show(expr3)

end I04EnvRecursiveLanguage