object I06EnvHigherOrder 
  enum Expr 
    case C(c: BigInt)
    case N(name: String) 
    case BinOp(op: BinOps, arg1: Expr, arg2: Expr) 
    case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
    case Call(function: Expr, arg: Expr)
    case Fun(param: String, body: Expr)
  end Expr
  import Expr._

  enum BinOps
    case Plus, Minus, Times, Power, LessEq
  import BinOps._

  enum Value
    case I(i: BigInt)
    case F(f: Value => Value)

  def evalBinOp(op: BinOps)(x: Value, y: Value): Value = 
    import Value._
    (op,x,y) match
      case (Plus,I(x),I(y)) =>  I(x + y)
      case (Minus,I(x),I(y)) => I(x - y)
      case (Times,I(x),I(y)) => I(x * y)
      case (Power,I(x),I(y)) => I(x.pow(y.toInt))
      case (LessEq,I(x),I(y)) => I(if (x <= y) 1 else 0)
      case _ => error(s"Type error in $x ${strOp(op)} $y")
  end evalBinOp

  type Env = Map[String, Value]
  val emptyEnv: Env = Map()

  def minus(e1: Expr, e2: Expr) = BinOp(Minus, e1, e2)
  def plus(e1: Expr, e2: Expr) = BinOp(Plus, e1, e2)
  def leq(e1: Expr, e2: Expr) = BinOp(LessEq, e1, e2)
  def times(e1: Expr, e2: Expr) = BinOp(Times, e1, e2)

  val defs = Map[String, Expr](
    "fact" -> Fun("n",
       IfNonzero(N("n"),
         times(N("n"), 
               Call(N("fact"), minus(N("n"), C(1)))),
         C(1))),
    "div" -> Fun("x", Fun("y",
       IfNonzero(BinOp(LessEq, N("y"), N("x")),
         plus(C(1), 
              Call(Call(N("div"), minus(N("x"), N("y"))),
                        N("y"))),
         C(0)))),
    "twice" -> Fun("f", Fun("x",
      Call(N("f"), Call(N("f"), N("x"))))),
    "twice1" -> Fun("f", Fun("fact",
      Call(N("f"), Call(N("f"), N("fact"))))), 
    "square" -> Fun("x", BinOp(Times, N("x"), N("x")))
  )
  val factExpr1 = Call(N("fact"), C(6))
  val divExpr2 = Call(Call(N("div"), C(15)), C(6))
  val expr3 = Call(Call(N("div"), factExpr1), C(10))
  val expr4 = Call(Call(N("twice"), N("square")), C(3))

  def evalEnv(e: Expr, env: Map[String, Value]): Value = e match
    case C(c) => Value.I(c)
    case N(n) => env.get(n) match
      case Some(v) => v
      case None => defs.get(n) match
        case None => error(s"Unknown name $n")
        case Some(body) => evalEnv(body, env)
    case BinOp(op, arg1, arg2) =>
      evalBinOp(op)(evalEnv(arg1,env), evalEnv(arg2,env))
    case IfNonzero(cond, trueE, falseE) =>
      if evalEnv(cond,env) != Value.I(0) then evalEnv(trueE,env)
      else evalEnv(falseE,env)
    case Fun(n,body) => Value.F{(v: Value) =>
      evalEnv(body, env + (n -> v)) }
    case Call(fun, arg) => evalEnv(fun,env) match 
      case Value.F(f) => f(evalEnv(arg,env))
      case _ => error(s"Non-function arg $fun in $fun($arg)")
  
  def error(s: String): Nothing = throw Exception(s)

  // Printing and displaying

  def strOp(op: BinOps): String = op match
    case Plus => "+"
    case Minus => "-"
    case Times => "*"
    case Power => "^"
    case LessEq => "<="
  
  def str(e: Expr): String = 
   e match 
    case C(c) => c.toString
    case N(n) => n    
    case BinOp(op, arg1, arg2) =>
      s"(${strOp(op)} ${str(arg1)} ${str(arg2)})"
    case IfNonzero(cond, trueE, falseE) =>
      s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"
    case Call(f, arg) => str(f) + "(" + str(arg) + ")"
    case Fun(n, body) => s"($n => ${str(body)})"
  end str

  def strEnv(env: Map[String, Expr]): String = 
    val defStrs = 
      for ((name,body) <- env)
      yield s"def $name =\n${str(body)}"
    defStrs.mkString("\n\n") + "\n"
  end strEnv

  def strVal(v: Value): String =
    v match 
      case Value.I(i) => i.toString
      case Value.F(f) => "<function>"

  def show(e: Expr, log: Boolean = false): Unit =
    if log then Logger.on else Logger.off
    println(str(e))
    println(" ~~> " + strVal(evalEnv(e,emptyEnv)) + "\n")
    if log then Logger.off

  def main(args: Array[String]): Unit =    
    println(strEnv(defs))
    show(Call(N("square"), Call(N("square"),C(3))))
    show(Call(N("fact"), C(0)))
    show(factExpr1)    
    show(divExpr2, true)
    show(expr3)    
    show(expr4, true)  
    show(Call(Call(N("twice"), N("fact")), C(3)), true) 
    show(Call(Call(N("twice1"), N("fact")), C(3)), true) // var capture not an issue

end I06EnvHigherOrder