package sims

abstract class Simulation with

  type Action = () => Unit

  case class Event(time: Int, action: Action)

  private var curtime = 0
  def currentTime: Int = curtime

  private var agenda: List[Event] = List()

  private def insert(items: List[Event], item: Event): List[Event] =
    items match
      case first :: rest if first.time <= item.time =>
        first :: insert(rest, item)
      case _ =>
        item :: items

  def afterDelay(delay: Int)(block: => Unit): Unit =
    val item = Event(currentTime + delay, () => block)
    agenda = insert(agenda, item)

  private def loop(): Unit = agenda match
    case first :: rest =>
      agenda = rest
      curtime = first.time
      first.action()
      loop()
    case Nil =>

  def run(): Unit =
    afterDelay(0) {
      println(s"*** simulation started, time = $currentTime ***")
    }
    loop()

end Simulation

abstract class Gates extends Simulation with
  def InverterDelay: Int
  def AndGateDelay: Int
  def OrGateDelay: Int

  class Wire with
    private var sigVal = false
    private var actions: List[Action] = List()

    def getSignal(): Boolean = sigVal

    def setSignal(s: Boolean): Unit =
      if s != sigVal then
        sigVal = s
        actions.foreach(_())

    def addAction(a: Action): Unit =
      actions = a :: actions
      a()
  end Wire

  def inverter(input: Wire, output: Wire): Unit =
    def invertAction(): Unit =
      val inputSig = input.getSignal()
      afterDelay(InverterDelay) {
        output.setSignal(!inputSig)
      }
    input.addAction(invertAction)

  def andGate(in1: Wire, in2: Wire, output: Wire) =
    def andAction() =
      val in1Sig = in1.getSignal()
      val in2Sig = in2.getSignal()
      afterDelay(AndGateDelay) {
        output.setSignal(in1Sig & in2Sig)
      }
    in1.addAction(andAction)
    in2.addAction(andAction)

  def orGate(in1: Wire, in2: Wire, output: Wire): Unit =
    def orAction() =
      val in1Sig = in1.getSignal()
      val in2Sig = in2.getSignal()
      afterDelay(OrGateDelay) {
        output.setSignal(in1Sig | in2Sig)
      }
    in1.addAction(orAction)
    in2.addAction(orAction)

  /** Design orGate in terms of andGate, inverter */
  def orGateALT(in1: Wire, in2: Wire, output: Wire): Unit = ???

  def probe(name: String, wire: Wire): Unit =
    def probeAction(): Unit =
      println(s"$name $currentTime value = ${wire.getSignal()}")
    wire.addAction(probeAction)
end Gates

abstract class Circuits extends Gates with

  def halfAdder(a: Wire, b: Wire, s: Wire, c: Wire) =
    val d, e = Wire()
    orGate(a, b, d)
    andGate(a, b, c)
    inverter(c, e)
    andGate(d, e, s)

  def fullAdder(a: Wire, b: Wire, cin: Wire, sum: Wire, cout: Wire) =
    val s, c1, c2 = Wire()
    halfAdder(a, cin, s, c1)
    halfAdder(b, s, sum, c2)
    orGate(c1, c2, cout)
end Circuits

trait Delays with
  def InverterDelay = 2
  def AndGateDelay = 3
  def OrGateDelay = 5
end Delays

object sim extends Circuits with Delays

@main def TestCircuit =
  import sim._
  val input1, input2, sum, carry = Wire()
  probe("sum", sum)
  probe("carry", carry)
  halfAdder(input1, input2, sum, carry)
  input1.setSignal(true)
  run()
  input2.setSignal(true)
  run()



